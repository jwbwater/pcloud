from setuptools import setup

setup(
    name='pcloud',
    version='0.1.0',
    description='Remote backup manager.',
    py_modules=['pcloud'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'click',
    ],
    entry_points={
        'console_scripts': [
            'pcloud = pcloud:cli'
        ]
    },
)
