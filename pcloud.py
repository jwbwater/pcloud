"""
Simple command line interface for mounting and unmounting pcloud.

Benefits:
- automatic creation of the mount point directory
- it always remembers to run rclone mount in the background
- no need to remember "fusermount -u mount-point-directory"
- automatic removal of the mount point directory
- use mv, cp, rm instead of rclone's copy, copyto, move, moveto, delete,
  deletefile

Commands:
pcloud mount
    creates a directory for the mount point and uses rclone to mount
    pcloud as a file system.

pcloud unmount
    uses rclone to unmount pcloud and removes the mount point directory
"""

import click
import os
import re
import subprocess
import sys


USER = os.environ['USER']


@click.group()
def cli():
    """ Command line interface for pcloud. """


@cli.command()
def mount():
    """ Mount pcloud as a file system. """
    directory = mount_point()
    if os.path.exists(directory):
        sys.exit(
            "Cannot create mount point, {} already exists.".format(directory))
    print("Creating {} directory as mount point.".format(directory))
    subprocess.run(["mkdir", directory])
    subprocess.run("rclone mount pcloud: {} &".format(directory), shell=True)


def mount_point():
    """ Create absolute path to mount point directory. """
    directory = "/home/{}/pcloud".format(USER)
    return directory


def unmount_directory():
    """
    Use regular expression to find rclone mount directory.
    This solution is robust to changes in mount_point(). It would continue to
    work if random characters were used to prevent collision with existing
    directories.
    """
    directory = None
    grep = subprocess.Popen(
        ["ps aux | grep -i rclone"],
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    stdout, err = grep.communicate()
    pattern = 'rclone mount pcloud: ([^\n]+)'
    m = re.search(pattern, stdout.decode())
    if m:
        directory = m.group(1)
    return directory


@cli.command()
def unmount():
    """ Unmount pcloud. """
    directory = unmount_directory()
    if directory and os.path.isdir(directory):
        subprocess.run(["fusermount", "-u", directory])
        subprocess.run(["rmdir", directory])
    else:
        print("Mount point directory not found.")
