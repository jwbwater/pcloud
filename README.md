# Command line interface for pcloud
Command line interface written in Python3 that makes browsing and moving files to and from pcloud easier.

## Installation
```
sudo dnf install rclone
git clone git@gitlab.com:jwbwater/pcloud.git
sudo pip3 install .
```

## Usage
It's very complicated.
```
pcloud mount
...
do your thang
...
pcloud unmount
```
